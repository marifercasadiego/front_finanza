import React, { useState, useEffect } from "react"
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import AddIcon from '@mui/icons-material/Add';

import FormTable from './FormTable';

export default function IncomeForm() {

    const [amount, setAmount] = useState("");
    const handleAmount = (e) => {
        setAmount(e.target.value)
    };

    const [increasingAmount, setincreasingAmount] = useState("");
    const handleIncreasingAmount = (e) => {
        setincreasingAmount(e.target.value)
    };
    const [incomeFrequency, setIncomeFrequency] = useState("");
    const handleIncomeFrequency = (e) => {
        setIncomeFrequency(e.target.value)
    };
    const [description, setDescription] = useState("");
    const handleDescription = (e) => {
        setDescription(e.target.value)
    };

    let id = 0;
    const [data, setData] = useState([]);
    const handleSave = () => {
        // Assign new data to array
        setData((oldData)=> [...oldData, {id: ++id,amount: amount, increasingAmount: increasingAmount, incomeFrequency: incomeFrequency, description: description}])

        console.log(data); //In here there are a lot of chances that we see 'data' as an empty array (see the useEffect below for more explanation)

        // Reset of all the data without refreshing the page
        setAmount("")
        setincreasingAmount("")
        setIncomeFrequency("")
        setDescription("")
    };

    const handleDeleteClick = (id) => {
      console.log("here")
      setData(data.filter((row) => row.id !== id));
    };

    // useEffect with 'data' as dependency in order to observe the modification of the state hook 'data'
    // We need to 'observe' it with useEffect because the assignment that useState performs is asynchronous
    useEffect(() => {
        console.log(data);
    }, [data]);

  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        Carga de ingresos
      </Typography>
      <Grid container spacing={3}>
        <Grid item xs={12} md={6}>
          <TextField
            required
            id="amount"
            label="Monto"
            helperText="Monto del ingreso"
            fullWidth
            variant="standard"
            type="number"
            onChange={handleAmount}
            value={amount}
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <TextField
            required
            id="aumento"
            label="Aumento ($$$)"
            helperText="Monto de dinero en el cual se produce el aumento"
            fullWidth
            variant="standard"
            type="number"
            onChange={handleIncreasingAmount}
            value={increasingAmount}
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <TextField
            required
            id="freqPago"
            label="Frecuencia de Cobro"
            helperText="Cada cuanto tiempo se cobra"
            fullWidth
            variant="standard"
            onChange={handleIncomeFrequency}
            value={incomeFrequency}
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <TextField
            required
            id="descripcion"
            label="Descripcion"
            helperText="Descripcion del ingreso"
            fullWidth
            variant="standard"
            onChange={handleDescription}
            value={description}
          />
        </Grid>
        <Grid item xs={12}>
            <Button variant="contained" color="primary" startIcon={<AddIcon />} onClick={handleSave}>
                Agregar Ingreso
            </Button>
        </Grid>
      </Grid>
        {/* Table */}
        <Grid container spacing={3} sx={{ p: 2, display: 'flex', flexDirection: 'column' }}>
            <Grid item xs={12}>
                <Paper sx={{ p: 2, display: 'flex', flexDirection: 'column' }}>
                    <FormTable data={data} handleDeleteClick={handleDeleteClick} />
                </Paper>
            </Grid>
        </Grid>
        {/* /Table */}
    </React.Fragment>
  );
}