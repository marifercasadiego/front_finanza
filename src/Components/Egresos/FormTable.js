import React from "react"
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import DeleteIcon from '@mui/icons-material/DeleteOutlined';
import Title from '../Title/Title';
import Button from '@mui/material/Button';

export default function FormTable({data, handleDeleteClick}) {

  return (
    <React.Fragment>
      <Title>Egresos</Title>
      <Table size="small">
        <TableHead>
          <TableRow>
            <TableCell>Fecha de carga</TableCell>
            <TableCell>Descripcion Monto</TableCell>
            <TableCell align="right">Monto</TableCell>
            <TableCell></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {data.map((row) => (
            <TableRow key={row.id}>
              <TableCell>{new Date().getDate()}/{new Date().getMonth() + 1}/{new Date().getFullYear()}</TableCell>
              <TableCell>{row.description}</TableCell>
              <TableCell align="right">{`$ ${row.amount}`}</TableCell>
              <TableCell>{row.incomeFrequency}</TableCell>
              <TableCell>
                <Button variant="contained" color="primary" startIcon={<DeleteIcon />} onClick={() => handleDeleteClick(row.id)}>
                    Borrar
                </Button>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </React.Fragment>
  );
}