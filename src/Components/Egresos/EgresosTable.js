import * as React from 'react';
import PropTypes from 'prop-types';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import AddIcon from '@mui/icons-material/Add';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/DeleteOutlined';
import SaveIcon from '@mui/icons-material/Save';
import CancelIcon from '@mui/icons-material/Close';
import {
  GridRowModes,
  DataGrid,
  GridToolbarContainer,
  GridActionsCellItem,
} from '@mui/x-data-grid';
import {
  randomId,
} from '@mui/x-data-grid-generator';

import Suma from '../Suma/Calculator/Calculator'

// Hardcoded rows values
const initialRows = [
  {
    id: randomId(),
    monto: '300000',
    aumento: '2500',
    frecuencia: "Mensual",
    descripcion: "Salario ppal"
  },
  { id: randomId(), monto: '15000', aumento: '0', frecuencia: "Unica vez", descripcion: "Sueldo" },
  { id: randomId(), monto: '5000', aumento: '5%', frecuencia: "Unica vez", descripcion: "Vacaciones" },
  { id: randomId(), monto: '1500000', aumento: '0', frecuencia: "Unica vez", descripcion: "Sueldo" },
  { id: randomId(), monto: '2500000', aumento: '0', frecuencia: "Unica vez", descripcion: "Regalo" },
  { id: randomId(), monto: '20000', aumento: '3%', frecuencia: "Unica vez", descripcion: "Sueldo" },
  { id: randomId(), monto: '4000', aumento: '0', frecuencia: "Unica vez", descripcion: "Sueldo" },
  { id: randomId(), monto: '3400', aumento: '20%', frecuencia: "Unica vez", descripcion: "Tarjeta" },
  { id: randomId(), monto: '2000', aumento: '0', frecuencia: "Unica vez", descripcion: "Transferencia" },
];

function EditToolbar(props) {

  const { setRows, setRowModesModel } = props;

  const handleClick = () => {
    const id = randomId();
    setRows((oldRows) => [...oldRows, { id, monto: '0', aumento: '0', frecuencia: '', descripcion: '', isNew: true }]);
    setRowModesModel((oldModel) => ({
      ...oldModel,
      [id]: { mode: GridRowModes.Edit, fieldToFocus: 'monto' },
    }));
  };

  return (
    <GridToolbarContainer sx={{ p: 2,}}>
      <Button variant="contained" color="primary" startIcon={<AddIcon />} onClick={handleClick}>
        Agregar Egreso
      </Button>
    </GridToolbarContainer>
  );
}

EditToolbar.propTypes = {
  setRowModesModel: PropTypes.func.isRequired,
  setRows: PropTypes.func.isRequired,
};

export default function EgresoTable() {
  const [rows, setRows] = React.useState(initialRows);
  const [rowModesModel, setRowModesModel] = React.useState({});

  const handleRowEditStart = (params, event) => {
    event.defaultMuiPrevented = true;
  };

  const handleRowEditStop = (params, event) => {
    event.defaultMuiPrevented = true;
  };

  const handleEditClick = (id) => () => {
    setRowModesModel({ ...rowModesModel, [id]: { mode: GridRowModes.Edit } });
  };

  const handleSaveClick = (id) => () => {
    setRowModesModel({ ...rowModesModel, [id]: { mode: GridRowModes.View } });
  };

  const handleDeleteClick = (id) => () => {
    setRows(rows.filter((row) => row.id !== id));
  };

  const handleCancelClick = (id) => () => {
    setRowModesModel({
      ...rowModesModel,
      [id]: { mode: GridRowModes.View, ignoreModifications: true },
    });

    const editedRow = rows.find((row) => row.id === id);
    if (editedRow.isNew) {
      setRows(rows.filter((row) => row.id !== id));
    }
  };

  const processRowUpdate = (newRow) => {
    const updatedRow = { ...newRow, isNew: false };
    setRows(rows.map((row) => (row.id === newRow.id ? updatedRow : row)));
    return updatedRow;
  };

  const columns = [
    { field: 'monto', headerName: 'Monto', type: 'number', width: 180, editable: true },
    { field: 'aumento', headerName: 'Aumento Estipulado', width: 180, type: 'number', editable: true },
    {
      field: 'frecuencia',
      headerName: 'Frecuencia Pago',
      type: "singleSelect", 
      valueOptions: ["Unica vez","Horas", "Diario", "Semanal", "Quincenal", "Mensual"],
      width: 220,
      editable: true,
    },
    { field: 'descripcion', headerName: 'Descripcion', width: 130, editable: true, },
    {
      field: 'actions',
      type: 'actions',
      headerName: 'Actions',
      width: 100,
      cellClassName: 'actions',
      getActions: ({ id }) => {
        const isInEditMode = rowModesModel[id]?.mode === GridRowModes.Edit;

        if (isInEditMode) {
          return [
            <GridActionsCellItem
              icon={<SaveIcon />}
              label="Save"
              onClick={handleSaveClick(id)}
            />,
            <GridActionsCellItem
              icon={<CancelIcon />}
              label="Cancel"
              className="textPrimary"
              onClick={handleCancelClick(id)}
              color="inherit"
            />,
          ];
        }

        return [
          <GridActionsCellItem
            icon={<EditIcon />}
            label="Edit"
            className="textPrimary"
            onClick={handleEditClick(id)}
            color="inherit"
          />,
          <GridActionsCellItem
            icon={<DeleteIcon />}
            label="Delete"
            onClick={handleDeleteClick(id)}
            color="inherit"
          />,
        ];
      },
    },
  ];

  const [pageSize, setPageSize] = React.useState(5);

  return (
    <>
    <Box
      sx={{
        height: 500,
        width: '100%',
        '& .actions': {
          color: 'text.secondary',
        },
        '& .textPrimary': {
          color: 'text.primary',
        },
      }}
    >

      <DataGrid
        pageSize={pageSize}
        onPageSizeChange={(newPageSize) => setPageSize(newPageSize)}
        rowsPerPageOptions={[5, 10, 20]}
        rows={rows}
        columns={columns}
        editMode="row"
        rowModesModel={rowModesModel}
        onRowEditStart={handleRowEditStart}
        onRowEditStop={handleRowEditStop}
        processRowUpdate={processRowUpdate}
        components={{
          Toolbar: EditToolbar,
        }}
        componentsProps={{
          toolbar: { setRows, setRowModesModel },
        }}
        experimentalFeatures={{ newEditingApi: true }}
      />
    </Box>
    <div>
    <Suma/>
    </div>
    </>
  );
}
