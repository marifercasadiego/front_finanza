import * as React from 'react';
import Typography from '@mui/material/Typography';
import Title from '../Title/Title';


export default function Deposits() {
  return (
    <React.Fragment>
      <Title>Egresos Recientes</Title>
      <Typography component="p" variant="h4">
        $7.2042,42
      </Typography>
      <Typography color="text.secondary" sx={{ flex: 1 }}>
      Septiembre, 2022
      </Typography>
      <div>
      </div>
    </React.Fragment>
  );
}