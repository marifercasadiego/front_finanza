import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Title from '../Title/Title';

function createData(id, date, name, shipTo, paymentMethod, amount) {
  return { id, date, name, shipTo, paymentMethod, amount };
}

const rows = [
  createData(
    0,
    '16 Mar, 2019',
    'Maria Perez',
    'Tupelo, MS',
    'VISA ⠀•••• 3719',
    14452.44,
  ),
  createData(
    1,
    '16 Mar, 2019',
    'Daniela Muñoz',
    'London, UK',
    'VISA ⠀•••• 2574',
    86621.99,
  ),
  createData(
    2, 
    '16 Mar, 2019', 
    'Tom Scholz', 
    'Boston, MA', 
    'MC ⠀•••• 1253', 
    5400.81
  ),
];

export default function Orders() {
  return (
    <React.Fragment>
      <Title>Órdenes recientes</Title>
      <Table size="small">
        <TableHead>
          <TableRow>
            <TableCell>Fecha</TableCell>
            <TableCell>Nombre</TableCell>
            <TableCell>Envie a</TableCell>
            <TableCell>Metodo de pago</TableCell>
            <TableCell align="right">Monto</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow key={row.id}>
              <TableCell>{row.date}</TableCell>
              <TableCell>{row.name}</TableCell>
              <TableCell>{row.shipTo}</TableCell>
              <TableCell>{row.paymentMethod}</TableCell>
              <TableCell align="right">{`$${row.amount}`}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </React.Fragment>
  );
}