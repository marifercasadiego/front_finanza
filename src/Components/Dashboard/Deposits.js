import * as React from 'react';
import Typography from '@mui/material/Typography';
import Title from '../Title/Title';

function preventDefault(event) {
  event.preventDefault();
}

export default function Deposits() {
  return (
    <React.Fragment>
      <Title>Depósitos Recientes</Title>
      <Typography component="p" variant="h4">
        $13.2042,42
      </Typography>
      <Typography color="text.secondary" sx={{ flex: 1 }}>
      Septiembre, 2022
      </Typography>
      <div>
      </div>
    </React.Fragment>
  );
}