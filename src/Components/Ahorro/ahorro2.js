import React, { useState } from "react";

const defaultState = {
  numero: ""
};

function Row({ onChange, onRemove, numero }) {
  return (
    <div>
      <input
        type="number"
        value={numero}
        onChange={(e) => onChange("numero", e.target.value)}
      />

      <button onClick={onRemove}>Eliminar</button>
    </div>
  );
}

export default function Pruebas() {
  const [rows, setRows] = useState([defaultState]);
  const [amount, setAmount] = useState(0);

  const handleOnChange = (index, name, value) => {
    const copyRows = [...rows];
    copyRows[index] = {
      ...copyRows[index],
      [name]: value
    };
    setRows(copyRows);
  };

  const handleOnAdd = () => {
    let total = 0;
    rows.map(function(num) {
      total = total + parseInt(num.numero);
    });
    console.log(total);
    setAmount(total);
    setRows(rows.concat(defaultState));
  };

  const handleOnRemove = (index) => {
    setAmount(amount - parseInt(rows[index].numero));
    const copyRows = [...rows];
    copyRows.splice(index, 1);
    setRows(copyRows);
  };
  console.log(amount);
  return (
    <div>
      {rows.map((row, index) => (
        <Row
          {...row}
          onChange={(name, value) => handleOnChange(index, name, value)}
          onRemove={() => handleOnRemove(index)}
          key={index}
        />
      ))}
      <button onClick={handleOnAdd}>Agregar</button>

      <input type="number" placeholder="suma total" value={amount} />
    </div>
  );
}
