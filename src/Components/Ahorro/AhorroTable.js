import * as React from 'react';
import Box from '@mui/material/Box';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/DeleteOutlined';
import SaveIcon from '@mui/icons-material/Save';
import CancelIcon from '@mui/icons-material/Close';
import {
  GridRowModes,
  DataGrid,
  GridActionsCellItem
} from '@mui/x-data-grid';
import {
  randomId,
} from '@mui/x-data-grid-generator';

const initialRows = [
  {
    id: randomId(),
    ingreso: '300000',
    egreso: '2500',
    frecuencia: "Mensual",
    descripcion: "Salario ppal"
  },
  { id: randomId(), ingreso: '15000', egreso: '234' },
  { id: randomId(), ingreso: '5000', egreso: '234'},
  { id: randomId(), ingreso: '1500000', egreso: '324'},
  { id: randomId(), ingreso: '2500000', egreso: '3432'},
  { id: randomId(), ingreso: '20000', egreso: '234'},
  { id: randomId(), ingreso: '4000', egreso: '234'},
  { id: randomId(), ingreso: '3400', egreso: '324' },
  { id: randomId(), ingreso: '2000', egreso: '234'},
];


export default function Ahorro() {
  const [rows, setRows] = React.useState(initialRows);
  const [rowModesModel, setRowModesModel] = React.useState({});
  const [rowss, setRowss] = React.useState([defaultState]);
  const [amount, setAmount] = React.useState(0);

  const defaultState = {
    numero: ""
  };
  
  function Row({ onChange, onRemove, numero }) {
    return (
      <div>
        <input
          type="number"
          value={numero}
          onChange={(e) => onChange("numero", e.target.value)}
        />
  
        <button onClick={onRemove}>Eliminar</button>
      </div>
    );
  }

  const handleOnChange = (index, name, value) => {
    const copyRows = [...rowss];
    copyRows[index] = {
      ...copyRows[index],
      [name]: value
    };
    setRows(copyRows);
  };

  const handleOnAdd = () => {
    let total = 0;
    rowss.map(function(num) {
      total = total + parseInt(num.numero);
    });
    console.log(total);
    setAmount(total);
    setRowss(rowss.concat(defaultState));
  };

  const handleOnRemove = (index) => {
    setAmount(amount - parseInt(rowss[index].numero));
    const copyRows = [...rowss];
    copyRows.splice(index, 1);
    setRowss(copyRows);
  };


  const handleRowEditStart = (params, event) => {
    event.defaultMuiPrevented = true;
  };

  const handleRowEditStop = (params, event) => {
    event.defaultMuiPrevented = true;
  };

  const handleEditClick = (id) => () => {
    setRowModesModel({ ...rowModesModel, [id]: { mode: GridRowModes.Edit } });
  };

  const handleSaveClick = (id) => () => {
    setRowModesModel({ ...rowModesModel, [id]: { mode: GridRowModes.View } });
  };

  const handleDeleteClick = (id) => () => {
    setRows(rows.filter((row) => row.id !== id));
  };

  const handleCancelClick = (id) => () => {
    setRowModesModel({
      ...rowModesModel,
      [id]: { mode: GridRowModes.View, ignoreModifications: true },
    });

    const editedRow = rows.find((row) => row.id === id);
    if (editedRow.isNew) {
      setRows(rows.filter((row) => row.id !== id));
    }
  };

  const processRowUpdate = (newRow) => {
    const updatedRow = { ...newRow, isNew: false };
    setRows(rows.map((row) => (row.id === newRow.id ? updatedRow : row)));
    return updatedRow;
  };

  const columns = [
    { field: 'ingreso', headerName: 'Ingreso', type: 'number', width: 200, editable: true },
    { field: 'egreso', headerName: 'Egreso', width: 200, type: 'number', editable: true },
    { field: 'descripcion', headerName: 'Descripcion', width: 130, editable: true, },
    {
      field: 'actions',
      type: 'actions',
      headerName: 'Actions',
      width: 100,
      cellClassName: 'actions',
      getActions: ({ id }) => {
        const isInEditMode = rowModesModel[id]?.mode === GridRowModes.Edit;

        if (isInEditMode) {
          return [
            <GridActionsCellItem
              icon={<SaveIcon />}
              label="Save"
              onClick={handleSaveClick(id)}
            />,
            <GridActionsCellItem
              icon={<CancelIcon />}
              label="Cancel"
              className="textPrimary"
              onClick={handleCancelClick(id)}
              color="inherit"
            />,
          ];
        }

        return [
          <GridActionsCellItem
            icon={<EditIcon />}
            label="Edit"
            className="textPrimary"
            onClick={handleEditClick(id)}
            color="inherit"
          />,
          <GridActionsCellItem
            icon={<DeleteIcon />}
            label="Delete"
            onClick={handleDeleteClick(id)}
            color="inherit"
          />,
        ];
      },
    },
  ];


  return (
    <>
    <h3>Ahorro</h3>
    <Box
      sx={{
        height: 500,
        width: '100%',
        '& .actions': {
          color: 'text.secondary',
        },
        '& .textPrimary': {
          color: 'text.primary',
        },
      }}
    >
        <div>
      {rowss.map((row, index) => (
        <Row
          {...row}
          onChange={(name, value) => handleOnChange(index, name, value)}
          onRemove={() => handleOnRemove(index)}
          key={index}
        />
      ))}
      <button onClick={handleOnAdd}>Agregar</button>

      <input type="number" placeholder="suma total" value={amount} />
    </div>

      <DataGrid
        rowsPerPageOptions={[5, 10, 20]}
        rows={rowss}
        columns={columns}
        editMode="row"
        rowModesModel={rowModesModel}
        onRowEditStart={handleRowEditStart}
        onRowEditStop={handleRowEditStop}
        processRowUpdate={processRowUpdate}
        componentsProps={{
          toolbar: { setRowss, setRowModesModel },
        }}
        experimentalFeatures={{ newEditingApi: true }}
      />
    </Box>
    <div>
    </div>
    </>
  );
}
