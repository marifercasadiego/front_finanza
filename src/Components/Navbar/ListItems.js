import * as React from 'react';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import ListSubheader from '@mui/material/ListSubheader';

import MonetizationOnIcon from '@mui/icons-material/MonetizationOn';
import MoneyOffIcon from '@mui/icons-material/MoneyOff';
import TimelineIcon from '@mui/icons-material/Timeline';
import SsidChartIcon from '@mui/icons-material/SsidChart';
import DownhillSkiingIcon from '@mui/icons-material/DownhillSkiing';

import { Link } from "react-router-dom";

export const mainListItems = (
  <React.Fragment>
    <Link to="/incomesTable" style={{textDecoration: 'none', color: "inherit"}}>
      <ListItemButton>
        <ListItemIcon>
          <MonetizationOnIcon />
        </ListItemIcon>
        <ListItemText primary="Ingresos - Tabla" />
      </ListItemButton>
    </Link>
    <Link to="/incomesForm" style={{textDecoration: 'none', color: "inherit"}}>
      <ListItemButton>
        <ListItemIcon>
          <MonetizationOnIcon />
        </ListItemIcon>
        <ListItemText primary="Ingresos - Form" />
      </ListItemButton>
    </Link>
    <Link to="/expensesTable" style={{textDecoration: 'none', color: "inherit"}}>
      <ListItemButton>
        <ListItemIcon>
          <MoneyOffIcon />
        </ListItemIcon>
        <ListItemText primary="Egresos - Tabla" />
      </ListItemButton>
    </Link>
    <Link to="/expenses" style={{textDecoration: 'none', color: "inherit"}}>
      <ListItemButton>
        <ListItemIcon>
          <MoneyOffIcon />
        </ListItemIcon>
        <ListItemText primary="Egresos  - Form" />
      </ListItemButton>
    </Link>
  </React.Fragment>
);

export const secondaryListItems = (
  <React.Fragment>
    <ListSubheader component="div" inset>
      Estimaciones
    </ListSubheader>
    <Link to="/incomesreport" style={{textDecoration: 'none', color: "inherit"}}>
      <ListItemButton>
        <ListItemIcon>
          <TimelineIcon />
        </ListItemIcon>
        <ListItemText primary="Ingresos" />
      </ListItemButton>
    </Link>
    <Link to="/expensesreport" style={{textDecoration: 'none', color: "inherit"}}>
      <ListItemButton>
        <ListItemIcon>
          <DownhillSkiingIcon />
        </ListItemIcon>
        <ListItemText primary="Egresos" />
      </ListItemButton>
    </Link>
    <Link to="/card" style={{textDecoration: 'none', color: "inherit"}}>
      <ListItemButton>
        <ListItemIcon>
          <SsidChartIcon />
        </ListItemIcon>
        <ListItemText primary="Ahorros" />
      </ListItemButton>
    </Link>
  </React.Fragment>
);