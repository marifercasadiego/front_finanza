import * as React from 'react';
import { styled, useTheme } from '@mui/material/styles';
// Header
import MuiAppBar from '@mui/material/AppBar';
import Typography from '@mui/material/Typography';
import MenuIcon from '@mui/icons-material/Menu'; //npm install @mui/icons-material

// Sidebar
import Divider from '@mui/material/Divider';
import MuiDrawer from '@mui/material/Drawer';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import HomeIcon from '@mui/icons-material/Home';
import List from '@mui/material/List';
import { mainListItems, secondaryListItems } from './ListItems';

// Both
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';

// Router
import {Route, Routes} from "react-router-dom"
import { Link } from "react-router-dom";

// Components to route
import Dashboard from '../../Views/Dashboard';
import Dashboard2 from '../../Views/Dashboard2';
import Incomes1 from '../../Views/Incomes/Incomes1';
import Incomes2 from '../../Views/Incomes/Incomes2';
import Egresos1 from '../../Views/Egresos/Egresos1';
import Egresos2 from '../../Views/Egresos/Egresos2';
import Ahorro from '../../Views/Ahorro';
import Card from '../Tarjeta/Card'

const drawerWidth = 240;

// Sidebar opened/closed Format
const openedMixin = (theme) => ({
  width: drawerWidth,
  transition: theme.transitions.create('width', {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.enteringScreen,
  }),
  overflowX: 'hidden',
});

const closedMixin = (theme) => ({
  transition: theme.transitions.create('width', {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  overflowX: 'hidden',
  width: `calc(${theme.spacing(7)} + 1px)`,
  [theme.breakpoints.up('sm')]: {
    width: `calc(${theme.spacing(8)} + 1px)`,
  },
});
// /Content Format

// Sidebar format
const Drawer = styled(MuiDrawer, { shouldForwardProp: (prop) => prop !== 'open' })(
    ({ theme, open }) => ({
      width: drawerWidth,
      flexShrink: 0,
      whiteSpace: 'nowrap',
      boxSizing: 'border-box',
      ...(open && {
        ...openedMixin(theme),
        '& .MuiDrawer-paper': openedMixin(theme),
      }),
      ...(!open && {
        ...closedMixin(theme),
        '& .MuiDrawer-paper': closedMixin(theme),
      }),
    }),
  );
  
// Content format
const DrawerHeader = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'flex-end',
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
}));
// /Sidebar format

// Header format
const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== 'open',
})(({ theme, open }) => ({
  zIndex: theme.zIndex.drawer + 1,
  transition: theme.transitions.create(['width', 'margin'], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));
// /Header format


export default function Navbar() {
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <Box sx={{ display: 'flex' }}>
      <CssBaseline />
      {/* Header */}
      <AppBar position="fixed" open={open}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            sx={{
              marginRight: 5,
              ...(open && { display: 'none' }),
            }}
          >
            <MenuIcon />
          </IconButton>
          <Link to="/" style={{textDecoration: 'none', color: "inherit", }}>
              <IconButton
                color="inherit"
                edge="end"
                sx={{
                  fontSize:"large",
                }}
              >
                <HomeIcon />
              </IconButton>
            </Link>
          <Typography variant="h6" noWrap component="div" sx={{
                  p: 2,
                }}>
            Finanzas
          </Typography>
        </Toolbar>
      </AppBar>
      {/* Sidebar */}
      <Drawer variant="permanent" open={open}>
        <DrawerHeader>
            <IconButton onClick={handleDrawerClose}>
              {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
            </IconButton>
        </DrawerHeader>
        <Divider />
        <List component="nav">
            {mainListItems}
            <Divider sx={{ my: 1 }} />
            {secondaryListItems}
          </List>
      </Drawer>
      {/* Content */}
      <Box component="main" sx={{ flexGrow: 1, p: 0 }}> 
      {/* If i change the padding to 1 or 2 it makes the space between the content and the navbar bigger */}
        <DrawerHeader />
        <Routes>
                <Route exact path="/" element={<Dashboard />} /> 
                <Route path="/incomesTable" element={<Incomes1/>} />
                <Route path="/incomesForm" element={<Incomes2/>} />
                <Route path="/expensesTable" element={<Egresos1/>} />
                <Route path="/expenses" element={<Egresos2/>} />
                <Route path="/incomesreport" element={<Dashboard/>} />
                <Route path="/expensesreport" element={<Dashboard2/>} />
                <Route path="/card" element={<Card/>} />
            </Routes>
      </Box>
    </Box>
  );
}
