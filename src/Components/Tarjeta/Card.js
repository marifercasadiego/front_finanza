import React, { Component } from 'react';
import Cards from 'react-credit-cards';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import "./Card.css";

class Tarjeta extends Component {
  state = {
    cvc: '',
    expiry: '',
    focus: '',
    name: '',
    number: '',
  };

  handleInputFocus = (e) => {
    this.setState({ focus: e.target.name });
  }

  handleInputChange = (e) => {
    const { name, value } = e.target;

    this.setState({ [name]: value });
  }

  render() {
    return (
      <Card>
         <CardContent>
      <div id="PaymentForm">
        <Cards
          cvc={this.state.cvc}
          expiry={this.state.expiry}
          focused={this.state.focus}
          name={this.state.name}
          number={this.state.number}
        />
        <form>
          <input
            type="tel"
            name="number"
            placeholder="Numero de tarjeta"
            onChange={this.handleInputChange}
            onFocus={this.handleInputFocus}
            maxLength={16}
          />
          <input
            type="text"
            name="name"
            placeholder="Nombre"
            onChange={this.handleInputChange}
            onFocus={this.handleInputFocus}
            maxLength={19}
          />

          <input
            type="text"
            name="expiry"
            placeholder="expiracion"
            onChange={this.handleInputChange}
            onFocus={this.handleInputFocus}
            maxLength={4}
          />

          <input
            type="tel"
            name="cvc"
            placeholder="cvc"
            onChange={this.handleInputChange}
            onFocus={this.handleInputFocus}
            maxLength={3}
          />
        </form>
      </div>
      </CardContent>
      </Card>
    );
  }
}

export default Tarjeta;