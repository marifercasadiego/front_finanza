import React, {Component} from 'react';
import "./Display.css";

class Display extends Component {
    render(){
        return(
            <div>
                <h3>Calculadora</h3>
            <div className="Display">
                {this.props.data}
            </div>
            </div>
        );
    }
}

export default Display;