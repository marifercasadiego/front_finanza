import { useState, useEffect } from "react";

const Form1 = () => {
  const [name, setName] = useState("");

  useEffect(() => {
    /* Full localStorage
        The JSON.stringify() and JSON.parse() methods are actually something auxiliary, just in case the data
        stored contains some data type more complex than text (smth like a JSON object, wouldn't be weird at all). 
        -->> localStorage supports only key:value tuples, where datatypes for both of them has to be String 
      */
    //name?localStorage.setItem("name", JSON.stringify(name)):JSON.parse(localStorage.getItem("name"));
    
    // Basic localStorage
    // localStorage.setItem("name", JSON.stringify(name));

    // localStorage.setItem("name", JSON.stringify(name));
  }, [name]);


  return (
    <form>
      <input
        type="text"
        value={name}
        onChange={(e) => setName(e.target.value)}
        placeholder="Full name"
        aria-label="fullname"
      />
      <input type="submit" value="Submit"></input>
    </form>
  );
};

export default Form1;