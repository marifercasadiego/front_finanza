import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import reportWebVitals from './reportWebVitals';
import Navbar from "./Components/Navbar/Navbar";
import {BrowserRouter as Router} from "react-router-dom" //npm i react-router-dom

const routing = (
  // The routing process start analyzing the first route that is declared. In this case, every route will have the prefix / .If you have the same prefixes for several URL's, you should always use 'exact'
  // You might also use the Switch or the Routes component as a "father" of all the Route components
  <Router>
    {/* If you want a component to be shown in all the others components (smth like a navbar), then you add it above all the other components */}
    {/* I moved all the routing logic inside de Navbar component so that the component's tree is properly structured */}
    <Navbar/>
  </Router>
)

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    {routing}
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
