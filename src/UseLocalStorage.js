import Form1 from "./Form1";

const UseLocalStorage = () => {
    return (
      <div className="container">
        <h1>localStorage with React hooks</h1>
        <Form1 />
      </div>
    );
  };
  export default UseLocalStorage;
  